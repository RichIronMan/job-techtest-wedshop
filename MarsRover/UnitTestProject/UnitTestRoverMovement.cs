﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MarsRover.Models;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestRoverMovement
    {
        [TestMethod]
        public void TestLimitTurnLeft()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.N);
            t.TurnLeft();
            Assert.AreEqual(t.CurrentDirection, Rover.Direction.W);
        }
        [TestMethod]
        public void TestLimitTurnRight()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.N);
            t.TurnRight();
            Assert.AreEqual(t.CurrentDirection, Rover.Direction.E);
        }


        [TestMethod]
        public void TestMoveNorth()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.N);
            t.X = 1;
            t.Y = 1;
            t.MoveForward();

            Assert.AreEqual(t.Y, 2);
        }
        [TestMethod]
        public void TestMoveEast()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.E);
            t.X = 1;
            t.Y = 1;
            t.MoveForward();

            Assert.AreEqual(t.X, 2);
        }
        [TestMethod]
        public void TestMoveWest()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.W);
            t.X = 1;
            t.Y = 1;
            t.MoveForward();

            Assert.AreEqual(t.X, 0);
        }
        [TestMethod]
        public void TestMoveSouth()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.S);
            t.X = 1;
            t.Y = 1;
            t.MoveForward();

            Assert.AreEqual(t.Y, 0);
        }


        [TestMethod]
        public void TestAFewCommands()
        {
            var t = new MarsRover.Models.Rover();
            t.SetDirection(Rover.Direction.N);
            t.X = 1;
            t.Y = 1;
            t.MoveForward(); // x=1, y=2
            t.TurnRight();
            t.MoveForward(); // x=2, y=2
            t.MoveForward(); // x=3, y=2
            t.TurnLeft();
            t.MoveForward(); // x=3, y=3

            Assert.AreEqual(t.Y, 3);
            Assert.AreEqual(t.X, 3);
        }

    }
}
