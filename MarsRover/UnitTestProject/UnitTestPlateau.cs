﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestPlateau
    {
        [TestMethod]
        public void TestSetSize()
        {
            var p = new MarsRover.Models.Plateau();
            p.SetSize(100, 100);
            
            Assert.AreEqual(p.MaxX , 100);
            Assert.AreEqual(p.MaxY , 100);
        }
    }
}
