﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover.Models
{
    public class Plateau
    {
        public int MaxX;
        public int MaxY;

        public void SetSize(int maxX, int maxY)
        {
            MaxX = maxX;
            MaxY = maxY;
        }
    }

}
