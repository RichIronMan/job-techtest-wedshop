﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover.Models
{
    public class Rover
    {
        public int X;
        public int Y;
        
        public Direction CurrentDirection {
            get {
                if (EulerAngle == 0) return Direction.N;
                if (EulerAngle == 90) return Direction.E;
                if (EulerAngle == 180) return Direction.S;
                if (EulerAngle == 270) return Direction.W;
                return Direction.N;
            }
        }

        public int EulerAngle; // 0=North, 270= West, 90=East

        public void SetDirection(Direction dir)
        {
            if (dir == Direction.N) EulerAngle = 0;
            if (dir == Direction.E) EulerAngle = 90;
            if (dir == Direction.S) EulerAngle = 180;
            if (dir == Direction.W) EulerAngle = 270;
        }

        public void MoveForward()
        {
            if (CurrentDirection == Direction.N) Y += 1;
            if (CurrentDirection == Direction.S) Y -= 1;
            if (CurrentDirection == Direction.E) X += 1;
            if (CurrentDirection == Direction.W) X -= 1;
        }

        public void TurnLeft()
        {
            EulerAngle -= 90;
            if (EulerAngle == -90) // range restriction
                EulerAngle = 270;
        }

        public void TurnRight()
        {
            EulerAngle += 90;
            if (EulerAngle == 360) // range restriction
                EulerAngle = 0;
        }

        public enum Direction
        {
            N, E, S, W
        }
    }

}
