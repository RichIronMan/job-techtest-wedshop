﻿using MarsRover.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarsRover
{
    public partial class Form1 : Form
    {
        public List<string> CommandsSent;
        private Plateau MarsPateau ;

        public List<Rover> Rovers;

        public Form1()
        {
            InitializeComponent();
        }
        
        #region "Logging methods"

        private void ClearLog() => lstLog.Items.Clear();
        private void ClearOutputLog() => lstOutput.Items.Clear();
        private void AddToLog(string text)
        {
            lstLog.Items.Insert(0, text);
        }
        private void AddOutputToLog(string text)
        {
            lstOutput.Items.Add( text); // add so it's ordered like the doc wanted
        }

        #endregion


        private void btnBegin_Click(object sender, EventArgs e)
        {
            pnlControls.Enabled = true;

            // initialise
            ClearLog();
            ClearOutputLog();
            CommandsSent = new List<string>();
            MarsPateau = new Plateau();
            Rovers = new List<Rover>();
            AddToLog("Starting...");
                                   
        }

        private void btnSendCmd_Click(object sender, EventArgs e)
        {
            SendCommand(txtCommand.Text.Trim());
            // clear command after sending
            txtCommand.Text = "";
        }

        private void SendCommand(string command)
        {
            AddToLog(string.Format("Processing command: {0}", command));
            if (CommandsSent.Count == 0) // First command, so set the size
            {
                ProcessSetMapSize(command);
            }
            else
            {
                // if it's odd then it's a initial location of rover
                var cmdType = CommandsSent.Count % 2;
                if (cmdType == 0) // even -- so has move commands
                {
                    // get the last rover
                    var curRover = Rovers.Last();

                    ProcessCommandMoveRover(command, curRover);
                    
                }
                else // odd -- so set initial location
                {
                    // create a new rover
                    var newRover = new Rover();

                    ProcessCommandSetRoverLocation(command, newRover);

                    Rovers.Add(newRover);
                }
            }

            // save what was sent
            CommandsSent.Add(command);

        }


        #region "Process Command methods"

        // Could use polymorphism or a command pattern here, but no need for just  a few unique command lines


        // Set the width and height of the map
        // e.g. of command == "5 5"
        private void ProcessSetMapSize(string command)
        {
            // parse string
            var pieces = command.Split(' ');

            // TODO: should do more checking on a string before casting

            int x = int.Parse(pieces[0]);
            int y = int.Parse(pieces[0]);

            AddToLog(string.Format("Setting map size: {0}, {1}", x, y));

            MarsPateau.SetSize(x, y);
        }

        // e.g. "1 2 N"
        private void ProcessCommandSetRoverLocation(string command, Rover newRover)
        {
            AddToLog(string.Format("Setting Rovers' initial location"));

            // parse string
            var pieces = command.Split(' ');

            newRover.X = int.Parse(pieces[0]);
            newRover.Y = int.Parse(pieces[1]);
            newRover.SetDirection((Rover.Direction)Enum.Parse(typeof(Rover.Direction), pieces[2]));

            AddToLog(string.Format("Rover at: {0} x {1}", newRover.X, newRover.Y));

        }

        // e.g. "LMLMLMLMM"
        private void ProcessCommandMoveRover(string command, Rover curRover)
        {
            AddToLog(string.Format("Moving Rover"));

            // parse string
            foreach (var cha in command)
            {
                ProcessRoverMovement(cha, curRover);
            }

            // output final position of this rover
            string roverPos = string.Format("{0} {1} {2}", curRover.X ,  curRover.Y, curRover.CurrentDirection);
            Console.WriteLine(roverPos);
            AddOutputToLog(roverPos);
        }

        private void ProcessRoverMovement(char cha, Rover curRover)
        {
            switch (cha)
            {
                case 'L':
                    AddToLog("Rover turning Left");
                    curRover.TurnLeft();
                    break;
                case 'R':
                    AddToLog("Rover turning Right");
                    curRover.TurnRight();
                    break;
                case 'M':
                    AddToLog(string.Format("Rover Moving Forward in direction: {0}", curRover.CurrentDirection));
                    curRover.MoveForward();
                    break;
            }
            AddToLog(string.Format("Rover now at: {0} x {1}", curRover.X, curRover.Y));
        }


        #endregion

    }




}
